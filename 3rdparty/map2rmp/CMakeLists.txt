project(map2rmp)

# Required cmake version
cmake_minimum_required(VERSION 2.6.0)

find_package(GDAL REQUIRED)
find_package(PROJ REQUIRED)
find_package(JPEG REQUIRED)
find_package(Qt5Widgets REQUIRED)

set(SRCS
    argv.cpp
    main.cpp
    CFileGenerator.cpp
)

include_directories(
    ${CMAKE_BINARY_DIR}
    ${CMAKE_CURRENT_BINARY_DIR}
    ${GDAL_INCLUDE_DIRS}
    ${PROJ_INCLUDE_DIRS}
    ${JPEG_INCLUDE_DIRS}
)
if(WIN32)
    include_directories(
        ${CMAKE_SOURCE_DIR}/Win32/
    )
endif(WIN32)

link_directories(
    ${CMAKE_BINARY_DIR}/lib
)

add_executable( map2rmp
    ${SRCS}
)

if(APPLE)
set_target_properties( map2rmp PROPERTIES
    RUNTIME_OUTPUT_DIRECTORY ${MAC_BUNDLE_PATH}/Contents/Resources
)
endif(APPLE)

if(MSVC)
  add_definitions(-D_CRT_SECURE_NO_DEPRECATE )
endif(MSVC)

target_link_libraries( map2rmp
    ${GDAL_LIBRARIES}
    ${PROJ_LIBRARIES}
    ${JPEG_LIBRARIES}
    ${QT_LIBRARIES}
)

qt5_use_modules(map2rmp Widgets)

install(
    TARGETS
      map2rmp
    DESTINATION
      ${BIN_INSTALL_DIR}
)
